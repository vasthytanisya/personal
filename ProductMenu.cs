﻿using CashOut.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashOut.Menu
{
    public class ProductMenu
    {
        public ProductMenu()
        {
            var inputMenu = 0;
            do
            {
                DisplayProduct();

                Console.WriteLine("Product Menu");
                Console.WriteLine("1. Add Product");
                Console.WriteLine("2. Update Product");
                Console.WriteLine("3. Delete Product");
                Console.WriteLine("4. Back");
                Console.WriteLine("Please input your choice: ");
                var input = int.TryParse(Console.ReadLine(), out int inputNumber);
                inputMenu = inputNumber;

                if (input)
                {
                    switch (inputNumber)
                    {
                        case 1:
                            AddProduct();
                            break;
                        case 2:
                            UpdateProduct();
                            break;
                        case 3:
                            DeleteProduct();
                            break;
                    }
                }
            } while (inputMenu != 4);
            
        }

        private string ValidateProductCode()
        {
            var isValidProductCode = false;
            var productCode = string.Empty;
            do
            {
                Console.WriteLine("Please write your product code: ");
                var productCodeInput = Console.ReadLine();

                if(productCodeInput.Length < 2)
                {
                    Console.Write("The Product Code length must be 2 or grerater");
                }
                else
                {
                    var isExist = ProductData.ValidateExistProductCode(productCodeInput);
                    if (!isExist)
                    {
                        productCode = productCodeInput;
                        isValidProductCode = true;
                    }
                }
            } while (!isValidProductCode);

            return productCode;
        } 

        private string ValidateProductName()
        {
            var isValidProductName = false;
            var productName = string.Empty;
            do
            {
                Console.WriteLine("Please write your product name: ");
                var productNameInput = Console.ReadLine();

                if (productNameInput.Length < 2)
                {
                    Console.Write("The Product Code length must be 2 or grerater");
                }
                else
                {
                    productName = productNameInput;
                    isValidProductName = true;
                }
            } while (!isValidProductName);

            return productName;
        }

        private int ValidateProductStock()
        {
            var isValidProductStock = false;
            var productStock = 0;
            do
            {
                Console.WriteLine("Please write your product stock: ");
                var productStockInput = int.TryParse(Console.ReadLine(), out int stockInput);

                if (productStockInput)
                {
                    if (stockInput == 0)
                    {
                        Console.WriteLine("The Product Stock must be greater than 0");
                    }
                    else
                    {
                        productStock = stockInput;
                        isValidProductStock = true;
                    }
                }
            } while (!isValidProductStock);

            return productStock;
        }

        private decimal ValidateProductPrice()
        {
            var isValidProductPrice = false;
            var productPrice = 0M;

            do
            {
                Console.WriteLine("Please write your product price: ");
                var productPriceInput = decimal.TryParse(Console.ReadLine(), out decimal priceInput);

                if (productPriceInput)
                {
                    if (priceInput <= 100)
                    {
                        Console.WriteLine("The Product Price must be greater than 100");
                    }
                    else
                    {
                        productPrice = priceInput;
                        isValidProductPrice = true;
                    }
                }
            } while (!isValidProductPrice);

            return productPrice;
        }
       
        public void AddProduct()
        {
            var productCode = this.ValidateProductCode();
            var productName = this.ValidateProductName();
            var productStock = this.ValidateProductStock();
            var productPrice = this.ValidateProductPrice();

            ProductData.Products.Add(new Model.ProductModel
            {
                ProductCode = productCode,
                ProductName = productName,
                ProductStock = productStock,
                ProductPrice = productPrice,
            });
        }
    
        public void DisplayProduct()
        {
            Console.WriteLine("Product List:");
            foreach(var p in ProductData.Products)
            {
                Console.WriteLine($"| {p.ProductCode} | {p.ProductName} | {p.ProductStock} | {p.ProductPrice}");
            }
        }
   
        public void UpdateProduct()
        {
            var isExist = false;
            var productCode = string.Empty;
            
            do
            {
                Console.WriteLine("Please write product code (type exit to return): ");
                var productCodeInput = Console.ReadLine();

                if(productCodeInput == "exit")
                {
                    return;
                }

                var isExistProductCode = ProductData.ValidateExistProductCode(productCodeInput);

                if (isExistProductCode)
                {
                    productCode = productCodeInput;
                    isExist = true;
                }else
                {
                    Console.WriteLine("The Product Code was not found. Please re-enter your Product Code.");
                }
            } while (!isExist);

            var isValidProductName = false;
            var productName = string.Empty;
            do
            {
                Console.WriteLine("Please write your product name: ");
                var productNameInput = Console.ReadLine();

                if (productNameInput.Length < 2)
                {
                    Console.Write("The Product Code length must be 2 or grerater");
                }
                else
                {
                    productName = productNameInput;
                    isValidProductName = true;
                }
            } while (!isValidProductName);

            var isValidProductStock = false;
            var productStock = 0;
            do
            {
                Console.WriteLine("Please write your product stock: ");
                var productStockInput = int.TryParse(Console.ReadLine(), out int stockInput);

                if (productStockInput)
                {
                    if (stockInput == 0)
                    {
                        Console.WriteLine("The Product Stock must be greater than 0");
                    }
                    else
                    {
                        productStock = stockInput;
                        isValidProductStock = true;
                    }
                }
            } while (!isValidProductStock);

            var isValidProductPrice = false;
            var productPrice = 0M;

            do
            {
                Console.WriteLine("Please write your product price: ");
                var productPriceInput = decimal.TryParse(Console.ReadLine(), out decimal priceInput);

                if (productPriceInput)
                {
                    if (priceInput <= 100)
                    {
                        Console.WriteLine("The Product Price must be greater than 100");
                    }
                    else
                    {
                        productPrice = priceInput;
                        isValidProductPrice = true;
                    }
                }
            } while (!isValidProductPrice);

            var updateProduct = ProductData.Products.Where(p => p.ProductCode == productCode).First();
            updateProduct.ProductName = productName;
            updateProduct.ProductPrice = productPrice;
            updateProduct.ProductStock = productStock;
        }
    
        public void DeleteProduct()
        {
            var isExist = false;
            var productCode = string.Empty;

            do
            {
                Console.WriteLine("Please write product code (type exit to return): ");
                var productCodeInput = Console.ReadLine();

                if (productCodeInput == "exit")
                {
                    return;
                }

                var isExistProductCode = ProductData.ValidateExistProductCode(productCodeInput);

                if (isExistProductCode)
                {
                    productCode = productCodeInput;
                    isExist = true;
                }
                else
                {
                    Console.WriteLine("The Product Code was not found. Please re-enter your Product Code.");
                }
            } while (isExist);

            var removeProduct = ProductData.GetProduct(productCode);
            ProductData.Products.Remove(removeProduct);
        }
    }
}
